import { useEffect } from "react";
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import { useDispatch } from "react-redux";
import Header from './cmp/Header';
import ProductsList from './cmp/ProductsList';
import LikedList from "./cmp/LikedList";
import AddedList from "./cmp/AddedList";
import { fetchData } from './storage/actions';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Header 
        background="#FFFEFA"
      />
      <Routes>
        <Route
          path="/"
          element={<ProductsList/>}
        />
        <Route
          path="/liked"
          element={<LikedList/>}
        />
        <Route
          path="/added"
          element={<AddedList/>} 
        />
      </Routes>
    </ BrowserRouter>
  );
}

export default App;