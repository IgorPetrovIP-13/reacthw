import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import { useSelector } from 'react-redux';
import { selectLikedProducts } from '../storage/custom_hook';


const LikedList = () => {

  const products = useSelector(selectLikedProducts);

    return (
      <div className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
      </div>
    )
}

export default LikedList;