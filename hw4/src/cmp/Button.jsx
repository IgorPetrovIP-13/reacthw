import styles from './cmpStyles/Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({background, text, onClick}) => {
    return (
        <button style={{backgroundColor: background}} onClick={onClick} className={styles.button}>
            {text}
        </button>
    )
}

Button.propTypes = {
    background: PropTypes.string.isRequired, 
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Button
