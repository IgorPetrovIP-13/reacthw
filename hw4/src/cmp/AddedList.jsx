import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import { useSelector } from 'react-redux';
import { selectAddedProducts } from '../storage/custom_hook';


const AddedList = () => {

    const pruducts = useSelector(selectAddedProducts);

    return (
      <div className={styles.list}>
        {pruducts.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
      </div>
    )
}

export default AddedList;