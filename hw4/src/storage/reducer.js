const initialState = {
    products: [],
    likedProducts: [],
    addedProducts: []
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_PRODUCTS_SUCCESS':
        return { ...state, products: action.payload };
      case 'ADD_TO_LIKED_PRODUCTS':
        return { ...state, likedProducts: [...state.likedProducts, action.payload]};
      case 'ADD_TO_ADDED_PRODUCTS':
        return { ...state, addedProducts: [...state.addedProducts, action.payload]};
      case 'REMOVE_FROM_LIKED_PRODUCTS':
        return { ...state, likedProducts: state.likedProducts.filter((element => element !== action.payload))}
      case 'REMOVE_FROM_ADDED_PRODUCTS':
        return { ...state, addedProducts: state.addedProducts.filter((element => element !== action.payload))}
      default:
        return state;
    }
};
  
export default rootReducer;