import styles from './cmpStyles/Modal.module.scss'
import {darkenHexColor} from './common.js'

const Modal = ({background, header, closeButton=true, text, actions, closeFunc}) => {
    return (
        <div className={styles.modalWrapper} onClick={closeFunc}>
            <div style={{backgroundColor: background}} className={styles.modalContent} onClick={e => e.stopPropagation()}>
                <div style={{backgroundColor: darkenHexColor(background, 15)}} className={styles.modalHeader}>
                        {header}
                        {closeButton? <button className={styles.closeBtn} onClick={closeFunc}>⨉</button> : null}
                </div>
                <div className={styles.modalText}>{text}</div>
                <div className={styles.btnWrapper}>
                    {Object.keys(actions).map(key => (
                        <button style={{backgroundColor: darkenHexColor(background, 20)}}
                         className={styles.modalBtn} onClick={actions[key]}>{key}</button>))}
                </div>
                </div>
        </div>
    )
}

export default Modal
