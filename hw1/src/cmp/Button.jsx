import styles from './cmpStyles/Button.module.scss'

const Button = ({background, text, onClick}) => {
    return (
        <button style={{backgroundColor: background}} onClick={onClick} className={styles.button}>
            {text}
        </button>
    )
}

export default Button
