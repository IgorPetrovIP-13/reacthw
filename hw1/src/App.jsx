import { useState } from 'react';
import Button from './cmp/Button';
import Modal from './cmp/Modal';

function App() {
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  return (
    <main>
      <Button
        background="#E6676B"
        text="Open first modal"
        onClick={() => setIsDeleteModalOpen(true)}
      />
      <Button
        background="#3CB371"
        text="Open second modal"
        onClick={() => setIsAddModalOpen(true)}
      />
      {
        isDeleteModalOpen && 
        <Modal
        background='#ea4b35'
        header='Do you want to delete this file?'
        closeButton={true}
        text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
        actions={{
          'Ok': () => setIsDeleteModalOpen(false),
          'Cancel': () => setIsDeleteModalOpen(false)
        }}
        closeFunc={() => {setIsDeleteModalOpen(false)}}
        />
      }
      {
        isAddModalOpen && 
        <Modal
        background='#3CB371'
        header='Do you want to add new file?'
        closeButton={true}
        text="New file will be added to DB"
        actions={{
          'Ok': () => setIsAddModalOpen(false),
          'Cancel': () => setIsAddModalOpen(false)
        }}
        closeFunc={() => {setIsAddModalOpen(false)}}
        />
        }
    </main>
  );
}

export default App;
