import { useState, useEffect } from "react";
import Header from './cmp/Header';
import ProductsList from './cmp/ProductsList';

function App() {
  const [products, setProducts] = useState([]);
  const [likesValue, setLikesValue] = useState(0);
  const [addedValue, setAddedValue] = useState(0);
  const productsLink = '/products.json';

  useEffect(() => {
    localStorage.getItem('likes')
      ? setLikesValue(localStorage.getItem('likes'))
      : localStorage.setItem('likes', '0');

    localStorage.getItem('added')
      ? setAddedValue(localStorage.getItem('added'))
      : localStorage.setItem('added', '0');

    fetch(productsLink)
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(e => console.log(e));
  }, []);

  return (
    <main>
      <Header 
        background="#FFFEFA"
        likesValue={likesValue}
        addedValue={addedValue}/>
      <ProductsList
        products={products}
        setLikesValue={setLikesValue}
        setAddedValue={setAddedValue}/>
    </main>
  );
}

export default App;