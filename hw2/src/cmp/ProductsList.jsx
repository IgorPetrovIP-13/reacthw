import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import PropTypes from 'prop-types';

const ProductsList = ({ products, setLikesValue, setAddedValue}) => {
    return (
      <div className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
            setLikesValue={setLikesValue}
            setAddedValue={setAddedValue}
          />
        ))}
      </div>
    )
}

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  setLikesValue: PropTypes.func.isRequired,
  setAddedValue: PropTypes.func.isRequired
}

export default ProductsList;