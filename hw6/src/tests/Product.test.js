import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Product from '../cmp/Product';

const mockStore = configureStore([]);

test('Product component renders correctly', () => {
  const store = mockStore({
    likedProducts: ['12345'],
    addedProducts: ['12345'],
  });

  const productProps = {
    name: 'Sample Product',
    price: 100,
    imageURL: 'https://example.com/product.jpg',
    SKU: '12345',
    color: 'blue',
    description: 'This is a sample product description.',
  };

  const { container } = render(
    <Provider store={store}>
      <Product {...productProps} />
    </Provider>
  );

  expect(container).toMatchSnapshot();
});