import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import BuyingModal from '../cmp/BuyingModal';
import { waitFor } from '@testing-library/dom';

const mockStore = configureStore([]);

test('Submit button click calls onSubmit', async () => {
  const store = mockStore({
    addedProducts: [],
  });

  const mockCloseFunc = jest.fn();

  const { getByTestId, getByLabelText } = render(
    <Provider store={store}>
      <BuyingModal closeFunc={mockCloseFunc} />
    </Provider>
  );

  const nameInput = getByLabelText('Name');
  const surnameInput = getByLabelText('Surname');
  const ageInput = getByLabelText('Age');
  const addressInput = getByLabelText('Address');
  const phoneNumberInput = getByLabelText('Phone');

  fireEvent.focus(nameInput);
  fireEvent.focus(surnameInput);
  fireEvent.focus(ageInput);
  fireEvent.focus(addressInput);
  fireEvent.focus(phoneNumberInput);

  fireEvent.change(nameInput, { target: { value: 'John' } });
  fireEvent.change(surnameInput, { target: { value: 'Doe' } });
  fireEvent.change(ageInput, { target: { value: '25' } });
  fireEvent.change(addressInput, { target: { value: '123 Main St' } });
  fireEvent.change(phoneNumberInput, { target: { value: '+380123456789' } });

  const submitButton = getByTestId('submit-button');
  fireEvent.click(submitButton);

  await waitFor(() => {
    expect(mockCloseFunc).toHaveBeenCalled();
  });
});