import React from 'react';
import Button from '../cmp/Button';
import { render, fireEvent } from '@testing-library/react';

test('Button component snapshot', () => {
  const component = (
    <Button
      background="#FF0000"
      text="Click me"
      onClick={() => {}}
      extra={{ fontWeight: 'bold' }}
    />
  );

  expect(component).toMatchSnapshot();
});

test('Button onClick event', () => {
    const onClickMock = jest.fn();
  
    const { getByText } = render(
      <Button
        background="#FF0000"
        text="Click me"
        onClick={onClickMock}
        extra={{ fontWeight: 'bold' }}
      />
    );
  
    const button = getByText('Click me');
    fireEvent.click(button);
  
    expect(onClickMock).toHaveBeenCalled();
  });