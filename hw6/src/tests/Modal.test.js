import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Modal from '../cmp/Modal';

test('Modal component renders correctly', () => {
    const background = '#FF0000';
    const header = 'Test Header';
    const closeButton = true;
    const text = 'Test Text';
    const actions = {
      Confirm: jest.fn(),
      Cancel: jest.fn(),
    };
    const closeFunc = jest.fn();
  
    const { getByText, queryByText } = render(
      <Modal
        background={background}
        header={header}
        closeButton={closeButton}
        text={text}
        actions={actions}
        closeFunc={closeFunc}
      />
    );
  
    expect(getByText(header)).toBeTruthy();
    expect(getByText(text)).toBeTruthy();
    expect(getByText('Confirm')).toBeTruthy();
    expect(getByText('Cancel')).toBeTruthy();
  
    if (closeButton) {
      fireEvent.click(getByText('⨉'));
      expect(closeFunc).toHaveBeenCalled();
    } else {
      expect(queryByText('⨉')).toBeNull();
    }
  
    fireEvent.click(getByText('Confirm'));
    expect(actions.Confirm).toHaveBeenCalled();
    fireEvent.click(getByText('Cancel'));
    expect(actions.Cancel).toHaveBeenCalled();
  });


  test('Modal component renders correctly', () => {
    const modalProps = {
      background: '#ffffff',
      header: 'Sample Modal',
      closeButton: true,
      text: 'This is a sample modal dialog.',
      actions: {
        Close: () => {},
        Save: () => {},
      },
      closeFunc: () => {},
    };
  
    const { container } = render(<Modal {...modalProps} />);
    expect(container).toMatchSnapshot();
  });