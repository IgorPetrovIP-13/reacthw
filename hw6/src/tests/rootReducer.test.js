import rootReducer from '../storage/reducer';

describe('rootReducer', () => {
  it('should return the initial state', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    expect(rootReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle FETCH_PRODUCTS_SUCCESS', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    const products = [{
        "Name": "iPhone 13 Pro",
        "Price": 999.99,
        "ImageURL": "https://itmag.ua/upload/iblock/d78/uc0hpmxpptppsyt3peof4ahug1zirp5k/186u.jpg",
        "SKU": "123456",
        "Color": "Graphite"
      }];
    const action = { type: 'FETCH_PRODUCTS_SUCCESS', payload: products };
    const expectedState = {
      products,
      likedProducts: [],
      addedProducts: [],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle ADD_TO_LIKED_PRODUCTS', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    const productToAdd = "123456";
    const action = { type: 'ADD_TO_LIKED_PRODUCTS', payload: productToAdd };
    const expectedState = {
      products: [],
      likedProducts: [productToAdd],
      addedProducts: [],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle ADD_TO_ADDED_PRODUCTS', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    const productToAdd = "123456";
    const action = { type: 'ADD_TO_ADDED_PRODUCTS', payload: productToAdd };
    const expectedState = {
      products: [],
      likedProducts: [],
      addedProducts: [productToAdd],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle REMOVE_FROM_LIKED_PRODUCTS', () => {
    const initialState = {
      products: [],
      likedProducts: ["123456"],
      addedProducts: [],
    };
    const productToRemove = "123456";
    const action = { type: 'REMOVE_FROM_LIKED_PRODUCTS', payload: productToRemove };
    const expectedState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle REMOVE_FROM_ADDED_PRODUCTS', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: ["123456"],
    };
    const productToRemove = "123456";
    const action = { type: 'REMOVE_FROM_ADDED_PRODUCTS', payload: productToRemove };
    const expectedState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle CLEAR_ADDED_PRODUCTS', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: ["123456"],
    };
    const action = { type: 'CLEAR_ADDED_PRODUCTS' };
    const expectedState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    expect(rootReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle unknown action type', () => {
    const initialState = {
      products: [],
      likedProducts: [],
      addedProducts: [],
    };
    const action = { type: 'UNKNOWN_ACTION_TYPE' };
    expect(rootReducer(initialState, action)).toEqual(initialState);
  });
});