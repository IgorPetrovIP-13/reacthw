export const fetchData = () => {
    const productsLink = '/products.json';
    return async (dispatch) => {
        try {
            const response = await fetch(productsLink);
            const data = await response.json();
            dispatch({ type: 'FETCH_PRODUCTS_SUCCESS', payload: data });
        }
        catch(e) {
            console.error('Error fetching products:', e);
            dispatch({ type: 'FETCH_PRODUCTS_FAILURE' });
        }
    }
}

export const addToLikedProducts = (value) => {
    return { type: 'ADD_TO_LIKED_PRODUCTS', payload: value };
};

export const addToAddedProducts = (value) => {
    return { type: 'ADD_TO_ADDED_PRODUCTS', payload: value };
};

export const removeFromLikedProducts = (value) => {
    return { type: 'REMOVE_FROM_LIKED_PRODUCTS', payload: value }
};

export const removeFromAddedProducts = (value) => {
    return { type: 'REMOVE_FROM_ADDED_PRODUCTS', payload: value }
};

export const clearAdded = () => {
    return { type: 'CLEAR_ADDED_PRODUCTS' }
}
