import { createSelector } from 'reselect';

export const memorizedProductsSelector = createSelector(
  [(state) => state.products],
  (products) => products
);

export const selectLikedProducts = createSelector(
  [(state) => state.products, (state) => state.likedProducts],
  (products, likedProducts) => {
    return products.filter((product) => likedProducts.includes(product.SKU));
  }
);

export const selectAddedProducts = createSelector(
  [(state) => state.products, (state) => state.addedProducts],
  (products, addedProducts) => {
    return products.filter((product) => addedProducts.includes(product.SKU));
  }
);