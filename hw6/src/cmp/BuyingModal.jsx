import { ErrorMessage, Field, Form, Formik } from "formik";
import styles from './cmpStyles/BuyingModal.module.scss';
import buttonStyle from './cmpStyles/Button.module.scss';
import { useDispatch, useSelector } from "react-redux";
import { clearAdded } from "../storage/actions";
import { PatternFormat } from 'react-number-format';
import * as Yup from 'yup';

const ValidationSchema = Yup.object().shape({
    name: Yup.string()
        .required("Enter your name"),
    surname: Yup.string()
        .required("Enter your surname"),
    age: Yup.number()
        .required("Enter your age")
        .min(18, "Inappropriate age")
        .max(100, "Inappropriate age"),
    address: Yup.string()
        .required("Enter your address"),
})  

const BasicForm = ({values, errors, handleChange, handleBlur}) => {
    return (
        <Form className={styles.form}>
            <div className={styles.container}>
                <div className={styles.inputWrapper}>
                    <Field id="nameForm" type="text" name="name" className={styles.input}/>
                    <label htmlFor="nameForm">Name</label>
                    <ErrorMessage name="name" component={'span'} className={styles.error}/>
                </div>
                <div className={styles.inputWrapper}>
                    <Field id="surnameForm" type="text" name="surname" className={styles.input}/>
                    <label htmlFor="surnameForm">Surname</label>
                    <ErrorMessage name="surname" component={'span'} className={styles.error}/>
                </div>
                <div className={styles.inputWrapper}>
                    <Field id="ageForm" type="number" name="age" className={styles.input}/>
                    <label htmlFor="ageForm">Age</label>
                    <ErrorMessage name="age" component={'span'} className={styles.error}/>
                </div>
                <div className={styles.inputWrapper}>
                    <Field id="addressForm" type="text" name="address" className={styles.input}/>
                    <label htmlFor="addressForm">Address</label>
                    <ErrorMessage name="address" component={'span'} className={styles.error}/>
                </div>
                <div className={styles.inputWrapper}>
                    <PatternFormat
                        id="phoneNumberForm"
                        name="phoneNumber"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={styles.input} 
                        format="+(380) ##-###-##-##" 
                        allowEmptyFormatting 
                        mask="_" 
                        onValueChange={(val) => {
                            values.phoneNumber = val.value;
                        }}
                    />
                    <label htmlFor="phoneNumberForm">Phone</label>
                    <ErrorMessage name="phoneNumber" component={'span'} className={styles.error}/>
                </div>
            </div>
           <button data-testid="submit-button" style={{backgroundColor: '#1e1e20'}} type="submit" className={buttonStyle.button}>Submit</button> 
        </Form>
    )
}

function BuyingModal ({closeFunc}) {

    const products = useSelector(state => state.addedProducts);
    const dispatch = useDispatch();

    return (
        <div className={styles.modalWrapper} onClick={closeFunc}>
            <div className={styles.modalContent} onClick={e => e.stopPropagation()}>
                <Formik
                    initialValues={{name: '', surname: '', age: '', address: '', phoneNumber: ''}}
                    validate={(values) => {
                        const errors = {};
                        const digitRegExp = /[0-9]/;
                        const addressRegExp = /[^A-Za-z0-9\s.,'-]+/;
                        if (digitRegExp.test(values.name)) {
                            errors.name = "Inappropriate name";
                        }
                        if (digitRegExp.test(values.surname)) {
                            errors.surname = "Inappropriate surname";
                        }
                        if (addressRegExp.test(values.address)) {
                            errors.address = "Inappropriate address";
                        }
                        if (values.phoneNumber.length < 9) {
                            errors.phoneNumber = "Inappropriate phone number";
                        }
                        return errors;    
                    }}
                    onSubmit={values => {
                        closeFunc();
                        console.log("Values", values)
                        console.log("Products SKU's: ", products);
                        dispatch(clearAdded())
                    }}
                    validationSchema={ValidationSchema}
                >
                {BasicForm}
                </Formik>
            </div>
        </div>
    )
}

export default BuyingModal;