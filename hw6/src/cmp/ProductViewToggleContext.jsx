import React, { createContext, useContext, useState } from 'react';

const ProductViewToggleContext = createContext();

export function useProductViewToggle() {
  return useContext(ProductViewToggleContext);
}

export function ProductViewToggleProvider({ children }) {
  const [isGridView, setIsGridView] = useState(true);

  const toggleView = () => {
    setIsGridView((prevValue) => !prevValue);
  };

  return (
    <ProductViewToggleContext.Provider value={{ isGridView, toggleView }}>
      {children}
    </ProductViewToggleContext.Provider>
  );
}