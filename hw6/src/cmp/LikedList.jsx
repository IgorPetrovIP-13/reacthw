import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import { useSelector } from 'react-redux';
import { selectLikedProducts } from '../storage/custom_hook';
import { useProductViewToggle } from './ProductViewToggleContext';

const LikedList = () => {

  const products = useSelector(selectLikedProducts);
  const { isGridView, _ } = useProductViewToggle();

    return (
      <div style={!isGridView ? {display: 'flex', flexDirection: 'column', width: '60vw'} : {}} className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
      </div>
    )
}

export default LikedList;