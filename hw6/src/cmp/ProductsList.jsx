import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import Button from './Button';
import { useSelector } from 'react-redux';
import { useProductViewToggle } from './ProductViewToggleContext';


const ProductsList = () => {
  const products = useSelector((state) => state.products);
  const { isGridView, toggleView } = useProductViewToggle();

    return (
      <>
      <Button 
        background='#1e1e20'
        text='TOGGLE VIEW'
        onClick={() => {toggleView()}}
        extra={{position: 'absolute', top: '6rem', right: '1.5rem'}}
      />
      <div style={!isGridView ? {display: 'flex', flexDirection: 'column', width: '60vw'} : {}} className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
      </div>
      </>
    )
}

export default ProductsList;