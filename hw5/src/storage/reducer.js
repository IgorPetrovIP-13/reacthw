const loadStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

const saveStateToLocalStorage = (state) => {
    localStorage.setItem('state', JSON.stringify(state));
};

const initialState = loadStateFromLocalStorage() || {
  products: [],
  likedProducts: [],
  addedProducts: [],
};


const rootReducer = (state = initialState, action) => {
    let newState;
    switch (action.type) {
      case 'FETCH_PRODUCTS_SUCCESS':
        return { ...state, products: action.payload };
      case 'ADD_TO_LIKED_PRODUCTS':
        newState = { ...state, likedProducts: [...state.likedProducts, action.payload]};
        break;
      case 'ADD_TO_ADDED_PRODUCTS':
        newState = { ...state, addedProducts: [...state.addedProducts, action.payload]};
        break;
      case 'REMOVE_FROM_LIKED_PRODUCTS':
        newState = { ...state, likedProducts: state.likedProducts.filter((element => element !== action.payload))};
        break;
      case 'REMOVE_FROM_ADDED_PRODUCTS':
        newState = { ...state, addedProducts: state.addedProducts.filter((element => element !== action.payload))};
        break;
      case 'CLEAR_ADDED_PRODUCTS':
        newState = { ...state, addedProducts: []};
        break;
      default:
        return state;
    }
    saveStateToLocalStorage(newState);
    return newState;
};
  
export default rootReducer;