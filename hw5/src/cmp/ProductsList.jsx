import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import { useSelector } from 'react-redux';


const ProductsList = () => {

  const products = useSelector((state) => state.products);

    return (
      <div className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
      </div>
    )
}

export default ProductsList;