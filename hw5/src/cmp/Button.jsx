import styles from './cmpStyles/Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({background, text, onClick, extra}) => {
    return (
        <button style={{backgroundColor: background, ...extra}} onClick={onClick} className={styles.button}>
            {text}
        </button>
    )
}

Button.propTypes = {
    background: PropTypes.string.isRequired, 
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    extra: PropTypes.object
}

export default Button
