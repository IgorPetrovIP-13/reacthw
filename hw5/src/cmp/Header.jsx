import styles from './cmpStyles/Header.module.scss';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from "react-redux";

const Header = ({ background }) => {

    const likesValue = useSelector((state) => state.likedProducts.length);
    const addedValue = useSelector((state) => state.addedProducts.length);

    return (
        <header className={styles.header} style={{backgroundColor: background}}>
            <div className={styles.imageWrapper}>
                <Link to='/liked'>
                    <img className={styles.image} src="/heart.svg" alt="heart" />
                    {Boolean(likesValue) && <div className={styles.counter}>{likesValue}</div>}
                </Link>
            </div>
            <div className={styles.imageWrapper}>
                <Link to='/added'>
                    <img className={styles.image} src="/basket.svg" alt="heart" />
                    {Boolean(addedValue) && <div className={styles.counter}>{addedValue}</div>}
                </Link>
            </div>
            <div className={styles.imageWrapper}>
                <Link to='/'><img className={styles.image} src="/home.svg" alt="home" /></Link>
            </div>
        </header>
    )
}

Header.propTypes = {
    background: PropTypes.string.isRequired
}

export default Header