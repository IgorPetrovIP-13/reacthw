import styles from './cmpStyles/ProductsList.module.scss';
import Product from './Product';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { selectAddedProducts } from '../storage/custom_hook';
import Button from './Button';
import BuyingModal from './BuyingModal';


const AddedList = () => {

    const products = useSelector(selectAddedProducts);
    const [isModalOpened, setIsModalOpened] = useState(false);

    return (
      <div>
        <div className={styles.list}>
        {products.map((product) => (
          <Product 
            key={product.SKU}
            name={product.Name}
            price={product.Price}
            imageURL={product.ImageURL}
            SKU={product.SKU}
            color={product.Color}
          />
        ))}
        </div>
        {products.length && <Button
          background='#1e1e20'
          text='BUY NOW'
          onClick={() => setIsModalOpened(true)}
          extra={{display: 'block', margin: 'auto'}}
        />} 
        {isModalOpened &&
        <BuyingModal
        closeFunc={() => setIsModalOpened(false)}
        />}
      </div>
    )
}

export default AddedList;