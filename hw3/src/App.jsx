import { useState, useEffect } from "react";
import Header from './cmp/Header';
import ProductsList from './cmp/ProductsList';
import { BrowserRouter, Routes, Route} from 'react-router-dom';

function App() {
  const [products, setProducts] = useState([]);
  const [likesValue, setLikesValue] = useState(0);
  const [addedValue, setAddedValue] = useState(0);
  const productsLink = '/products.json';

  useEffect(() => {
    console.log(1);
    localStorage.getItem('likes')
      ? setLikesValue(localStorage.getItem('likes'))
      : localStorage.setItem('likes', '0');

    localStorage.getItem('added')
      ? setAddedValue(localStorage.getItem('added'))
      : localStorage.setItem('added', '0');

    fetch(productsLink)
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(e => console.log(e));
  }, []);

  return (
    <BrowserRouter>
      <Header 
        background="#FFFEFA"
        likesValue={likesValue}
        addedValue={addedValue}
      />
      <Routes>
        <Route
          path="/"
          element={
            <ProductsList
              products={products}
              setLikesValue={setLikesValue}
              setAddedValue={setAddedValue}
            />
          }
        />
        <Route
          path="/liked"
          element={
            <ProductsList
              products={products.filter(product => localStorage.getItem(`${product.SKU}_liked`))}
              setLikesValue={setLikesValue}
              setAddedValue={setAddedValue}
              isPrice={false}
              isAdd={false}
            />
          }
        />
        <Route
          path="/added"
          element={
            <ProductsList
              products={products.filter(product => localStorage.getItem(`${product.SKU}_added`))}
              setLikesValue={setLikesValue}
              setAddedValue={setAddedValue}
              isLike={false}
            />
          } 
        />
      </Routes>
    </ BrowserRouter>
  );
}

export default App;