import styles from './cmpStyles/Header.module.scss';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const Header = ({background , addedValue, likesValue}) => {
    return (
        <header className={styles.header} style={{backgroundColor: background}}>
            <div className={styles.imageWrapper}>
                <Link to='/liked'>
                    <img className={styles.image} src="/heart.svg" alt="heart" />
                    {likesValue !== '0' ? <div className={styles.counter}>{likesValue}</div> : null}
                </Link>
            </div>
            <div className={styles.imageWrapper}>
                <Link to='/added'>
                    <img className={styles.image} src="/basket.svg" alt="heart" />
                    {addedValue !== '0' ? <div className={styles.counter}>{addedValue}</div> : null}
                </Link>
            </div>
            <div className={styles.imageWrapper}>
                <Link to='/'><img className={styles.image} src="/home.svg" alt="home" /></Link>
            </div>
        </header>
    )
}

Header.propTypes = {
    background: PropTypes.string.isRequired,
    addedValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    likesValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
}

export default Header