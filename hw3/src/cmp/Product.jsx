import { useState } from 'react';
import Button from './Button';
import Modal from './Modal';
import styles from './cmpStyles/Product.module.scss';
import PropTypes, { func } from 'prop-types';

const Product = ({name, price, imageURL, SKU, color, description, setLikesValue, setAddedValue, isPrice=true, isAdd=true, isLike=true}) => {

    const [isModalOpened, setIsModalOpened] = useState(false);
    const [isLiked, setIsLiked] = useState(localStorage.getItem(`${SKU}_liked`) === 'true');
    const [isAdded, setIsAdded] = useState(localStorage.getItem(`${SKU}_added`) === 'true');

    function addToCart () {
        setIsAdded(true);
        localStorage.setItem(`${SKU}_added`, 'true');
        localStorage.setItem('added', `${+localStorage.getItem('added') + 1}`)
        setAddedValue(`${+localStorage.getItem('added')}`);
    };

    function removeFromCart() {
        setIsAdded(false);
        localStorage.removeItem(`${SKU}_added`);
        localStorage.setItem('added', `${+localStorage.getItem('added') - 1}`)
        setAddedValue(`${+localStorage.getItem('added')}`);
    };

    function addToLiked() {
        setIsLiked(true);
        localStorage.setItem(`${SKU}_liked`, 'true');
        localStorage.setItem('likes', `${+localStorage.getItem('likes') + 1}`)
        setLikesValue(`${+localStorage.getItem('likes')}`);
    }

    function removeFromLiked() {
        setIsLiked(false);
        localStorage.removeItem(`${SKU}_liked`);
        localStorage.setItem('likes', `${+localStorage.getItem('likes') - 1}`)
        setLikesValue(`${+localStorage.getItem('likes')}`);
    }

    return (
        <div className={styles.wrapper}>
            {isLike && <svg onClick={!isLiked ? addToLiked : removeFromLiked} className={styles.like} width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M7.22303 0.665992C7.32551 0.419604 7.67454 0.419604 7.77702 0.665992L9.41343 4.60039C9.45663 4.70426 9.55432 4.77523 9.66645 4.78422L13.914 5.12475C14.18 5.14607 14.2878 5.47802 14.0852 5.65162L10.849 8.42374C10.7636 8.49692 10.7263 8.61176 10.7524 8.72118L11.7411 12.866C11.803 13.1256 11.5206 13.3308 11.2929 13.1917L7.6564 10.9705C7.5604 10.9119 7.43965 10.9119 7.34365 10.9705L3.70718 13.1917C3.47945 13.3308 3.19708 13.1256 3.25899 12.866L4.24769 8.72118C4.2738 8.61176 4.23648 8.49692 4.15105 8.42374L0.914889 5.65162C0.712228 5.47802 0.820086 5.14607 1.08608 5.12475L5.3336 4.78422C5.44573 4.77523 5.54342 4.70426 5.58662 4.60039L7.22303 0.665992Z" fill={isLiked ? 'blue' : 'currentColor'} /></svg>}
            <div className={styles.imageWrapper}>
                <div style={{backgroundColor: color }} className={styles.color}></div>
                <img className={styles.image} src={imageURL} alt={name} />
            </div>
            <h2 className={styles.heading}>{name}</h2>
            <p className={styles.description}>{description}</p>
            {isAdd && <div className={styles.addingWrapper}>
                {isPrice && <strong className={styles.price}>${price}</strong>}
                {!isAdded ?
                <Button 
                background={'#1e1e20'}
                text={'BUY'}
                onClick={() => {setIsModalOpened(true)}}/>
                :
                <Button 
                background={'#cc3434'}
                text={'REMOVE'}
                onClick={() => {setIsModalOpened(true)}}/>
                }
            </div>}
            {isModalOpened && isAdd && (!isAdded ? 
                <Modal
                background={'#1e1e20'}
                header={'Add to the cart?'}
                text={`${name} will be added to the cart`}
                actions={{
                    'OK': () => {setIsModalOpened(false); addToCart()},
                    'Cancel': () => setIsModalOpened(false)
                }}
                closeFunc={() => setIsModalOpened(false)}
                />
                :
                <Modal
                background={'#cc3434'}
                header={'Remove from the cart?'}
                text={`${name} will be removed from the cart`}
                actions={{
                    'OK': () => {setIsModalOpened(false); removeFromCart()},
                    'Cancel': () => setIsModalOpened(false)
                }}
                closeFunc={() => setIsModalOpened(false)}
                />)
            }
        </div>
    )
}

Product.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    SKU: PropTypes.string.isRequired,
    color: PropTypes.string,
    description: PropTypes.string,
    setLikesValue: PropTypes.func.isRequired,
    setAddedValue: PropTypes.func.isRequired,
    isPrice: PropTypes.bool,
    isAdd: PropTypes.bool,
    isLike: PropTypes.bool
};

Product.defaultProps = {
    color: 'white',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isPrice: true,
    isAdd: true,
    isLike: true
};

export default Product;